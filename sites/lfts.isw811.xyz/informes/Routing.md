# Routing


El routing es la manera en como el servidor permite navegar a través de la página o aplicación web, es la forma en que podemos acceder a los diferentes rutas donde se enuentran las vistas, la programación o simplemente el storage. Laravel posee un controlador que ayuda a la realización del routing o enrutamiento, cuenta con un lugar específico donde podemos colocar las las rutas relativas que llevan a las rutas absolutas de cada uno de los apartados que se necesitaran, el cual se encuentra en la carpeta routes, en el archivo web.php.

![Imagen](./images/routing.png "Imagen de la configuración de rutas")

## Video 1: Prueba

* Primeramente vamos a dublicar la función de direccionamento y la cambiaremos por test, posterior a eso creamos un archivo en views con el nombre test.blade.php y escribimos "Test Complete!" y probamos que la ruta funcione correctamente.

![Imagen](./images/routing-test.png "Imagen de la configuración de rutas")
![Imagen](./images/view-test.png "Imagen de la vista a la que apunta la anterior ruta")
![Imagen](./images/url-test.png "Imagen de la pagina web")

## Video 2: Envio de parametros

* También podemos enviar variables por este medio con un simple cambio en la configuración.Para esto necesitamos incializar la variable que se enviará por medio del url, es importante mencionar que el nombre debe ser igual para esta variable ya sea en la url, en su definición o en la vista, ya que es posible mostrar la variable en la vista. Dependiendo de como se desee mostrar se utilizará lo siguiente en la vista.

```php
{{ $variable }}      //si solo deseamos mostrar texto
{!! $variable !!}    //si se puede codificar el html
```
![Imagen](./images/sending-parameters.png "Imagen de la configuración de envio de parametros")

* si solo deseamos mostrar texto.

![Imagen](./images/view-parameters.png "Imagen de recepcción de parametros")
![Imagen](./images/url-parameters.png "Imagen de la pagina web")

* si se puede codificar el html.

![Imagen](./images/view-parameters2.png "Imagen de recepcción de parametros")
![Imagen](./images/url-parameters2.png "Imagen de la pagina web")


## Video 3: Wildcards

* Las wildcards son otra manera de enviar información por medio de la URL, simplemente cambia la sintaxis y la configuración del routing como se observa acontinuación. Para este ejemplo creamos una nueva vista llamada post.php.

![Imagen](./images/sending-wildcard.png "Imagen de la configuración de rutas")
![Imagen](./images/view-wildcard.png "Imagen de la vista ")
![Imagen](./images/url-wildcard1.png "Imagen de la pagina web")
![Imagen](./images/url-wildcard2.png "Imagen de la pagina web")
![Imagen](./images/url-wildcard3.png "Imagen de la pagina web")

## Video 4: Routing a controladores

* Para realizar la programaciión de diferentes configuraciones de url en una manera más ordenada y en proyectos grandes podemos utilizar controladores. Para hacer uso de estos hay dos formas, crear los controladores a mano o por medio de comandos.

* Primeramente vamos a configurar la ruta en una sola linea, con los parametros de URL, que en este caso se utilizará con wildcard, y el cotrolador acompañado de un signo "@" y el nombre de la función, como se observa acontinuación:

``` php
Route::get('/post/{post}', 'PostController@show');
```

![Imagen](./images/sending-controller.png "Imagen de la configuración de rutas")

* Y posteriormente crearemos el controlador.

### Crear controladores a mano

* En la ruta app/Http/controllers creamos el archivo con el nombre que se encigió anteriormente, en este caso PostsController, con la extensión .php y se deberá tener la siguiente estructura:

![Imagen](./images/Controller-Posts.png "Imagen del controller posts")

* Dentro de la clase escribiremos la función show con lo que deseemos que haga esta, normalmente las funciones con este nombre se utilizan para mostrar.

![Imagen](./images/Controller-Posts1.png "Imagen del controller posts")

### Crear controladores por linea de comandos

* En la terminal debemos dirigirnos hasta la ubicación del proyecto e ingresar el siguiente código bash.

``` bash
php artisian make:controller PostsController
```

* Cabe mencional que PostsController es el nombre de nuestro controlador, puedes cambiarlo a tu conveniencia y es necesario tener instalado php para realizar este comando 

![Imagen](./images/create-controller.png "Imagen del controller posts")

* como podemos ver en la siguiente imagen vemos que se creó un archivo similar al anterior, simplemente agregamos la función y lo que queremos que haga, recargamos la página y funciona de manera similar a la anterior, simplemente cambió la forma en que manejamos la información y el direccionamiento.

![Imagen](./images/Controller-Posts2.png "Imagen del controller posts")
![Imagen](./images/url-wildcard1.png "Imagen de la pagina web")


[Volver](../informes/Principal.md)