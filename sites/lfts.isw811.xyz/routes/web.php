<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// normal routing
Route::get('/', function () {
    return view('welcome');
});

// showing info
Route::get('/welcome', function () {
    return ['foo' => 'bar'];
});

//Sending information
Route::get('/test', function () {
    $name = request('name');

    return view('test', ['name' => $name]);
});

/**wildcards
Route::get('/post/{post}', function ($post) {
    $posts=[
        'my-first-post' => 'Hello, this is my first post!',
        'my-second-post' => 'Now I am getting the hang of this blogging thing'
    ];

    if (! array_key_exists($post, $posts)) {
        abort(404, 'Sorry that post was not found');
    }

    return view('post', [
        'post' => $posts[$post]
        ]);
});
**/

//Routing with controllers

Route::get('/post/{post}', 'PostsController@show');